from django.db import models


class Producto(models.Model):
    nombre = models.TextField(max_length=500)
    descripcion = models.TextField(max_length=5000)
    marca = models.CharField(max_length=255)
    modelo = models.CharField(max_length=255)
    categoria = models.ManyToManyField('Categoria')

    def dict_repr(self):
        return {
            'id': self.id,
            'label': self.nombre
        }
    
    def get_categorias(self):
        return self.categoria.all()

class Categoria(models.Model):
    codigo = models.CharField(max_length=255)
    nombre = models.TextField(max_length=500)