# Generated by Django 2.2 on 2019-12-18 08:51

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('billetera', '0003_remove_itembilletera_tarjeta'),
    ]

    operations = [
        migrations.AlterField(
            model_name='itembilletera',
            name='tipo',
            field=models.CharField(choices=[('Compra', 'Compra'), ('Venta', 'Venta'), ('Carga', 'Carga'), ('Retiro', 'Retiro')], default='Carga', max_length=255),
        ),
    ]
