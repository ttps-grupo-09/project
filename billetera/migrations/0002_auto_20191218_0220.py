# Generated by Django 2.2 on 2019-12-18 02:20

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('publicaciones', '0004_publicacion_user'),
        ('billetera', '0001_initial'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='billetera',
            name='cantidad',
        ),
        migrations.RemoveField(
            model_name='billetera',
            name='date_added',
        ),
        migrations.RemoveField(
            model_name='billetera',
            name='precio',
        ),
        migrations.AlterField(
            model_name='billetera',
            name='user',
            field=models.OneToOneField(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, to=settings.AUTH_USER_MODEL),
        ),
        migrations.CreateModel(
            name='ItemBilletera',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('cantidad', models.IntegerField(default=1)),
                ('date_added', models.DateTimeField(auto_now_add=True)),
                ('tipo', models.CharField(choices=[('Compra', 'Activa'), ('Venta', 'Inactiva'), ('Carga', 'Sin Stock')], default='Carga', max_length=255)),
                ('tarjeta', models.BooleanField(default=False)),
                ('billetera', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, to='billetera.Billetera')),
                ('publicacion', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, to='publicaciones.Publicacion')),
            ],
        ),
    ]
