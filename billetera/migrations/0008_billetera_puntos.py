# Generated by Django 2.2 on 2019-12-18 13:37

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('billetera', '0007_itembilletera_monto'),
    ]

    operations = [
        migrations.AddField(
            model_name='billetera',
            name='puntos',
            field=models.IntegerField(default=0),
        ),
    ]
