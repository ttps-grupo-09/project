from django.db import models
from usuarios.models import User
from publicaciones.models import Publicacion
from configuracion.models import Configuracion

class Billetera(models.Model):
    user = models.OneToOneField(User, blank=True, null=True, on_delete=models.SET_NULL)
    saldo = models.FloatField(default=0)
    puntos = models.IntegerField(default=0)

    def get_billetera_items(self):
        return self.itembilletera_set.all().order_by('-date_added')

    def add_item(self, data):
        """
        Recibe un dictionary con las siguientes características:
        - Tipo: proveniente del ItemBilletera
        - Descripcion: del movimiento
        - Monto: A restar o sumar del saldo de la billetera
        """
        item = ItemBilletera.Create_item(
            billetera=self,
            data=data
        )
        item.update_billetera_saldo()
        return item
    
    def incrementar_saldo(self, monto):
        self.saldo += monto
        self.save()
    
    def decrementar_saldo(self, monto):
        self.saldo -= monto
        self.save()
    
    def incrementar_puntos(self, monto_gastado):
        self.puntos += Configuracion.Get_config().get_puntos_por_peso(monto_gastado)
        self.save()

class ItemBilletera(models.Model):
    COMPRA = 'Compra'
    VENTA = 'Venta'
    CARGA = 'Carga'
    RETIRO = 'Retiro'
    TIPOS_CHOICES = [
        (COMPRA, 'Compra'),
        (VENTA, 'Venta'),
        (CARGA, 'Carga'),
        (RETIRO, 'Retiro'),
    ]

    CARGA_DESCRIPCION = 'Carga de Saldo'
    RETIRO_DESCRIPCION = 'Retiro de Saldo'

    billetera = models.ForeignKey(Billetera, blank=True, null=True, on_delete=models.SET_NULL)
    tipo = models.CharField(max_length=255, choices=TIPOS_CHOICES, default=CARGA)
    descripcion = models.TextField(max_length=1000, null=True)
    monto = models.FloatField()
    date_added = models.DateTimeField(auto_now_add=True)

    def serialize(self):
        return {
            'id': self.id,
            'tipo': self.tipo,
            'descripcion': self.descripcion,
            'fecha': self.date_added.date(),
            'monto': float("{0:.2f}".format(self.monto)),
            'is_positivo': self.is_positivo()
        }

    @classmethod
    def Create_item(cls, billetera, data):
        """
        Recibe un dictionary con las siguientes características:
        - Tipo: proveniente del ItemBilletera
        - Descripcion: del movimiento
        - Monto: A restar o sumar del saldo de la billetera
        """
        if data.get('tipo') == cls.VENTA:
            monto = Configuracion.Get_config().descontar_comision_plataforma(
                monto=data.get('monto')
            )
        else:
            monto = data.get('monto')
        return ItemBilletera.objects.create(
            billetera=billetera,
            tipo=data.get('tipo'),
            descripcion=data.get('descripcion'),
            monto=monto
        )

    def update_billetera_saldo(self):
        if self.is_positivo():
            self.billetera.incrementar_saldo(self.monto)
        else:
            self.billetera.decrementar_saldo(self.monto)

    def is_positivo(self):
        """
        Retorna si el movimiento incrementa el balance o
        lo decrementa.
        """
        if self.tipo == self.COMPRA or self.tipo == self.RETIRO:
            return False
        elif self.tipo == self.VENTA or self.tipo == self.CARGA:
            return True
