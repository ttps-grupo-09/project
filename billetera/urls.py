import billetera.views as views

from django.contrib import admin
from django.urls import path


urlpatterns = [
    path('', views.wallet_detail_view, name="billetera-list"),
]