from django.shortcuts import render
from django.contrib.auth.decorators import login_required
from billetera.models import *
from usuarios.models import *


@login_required
def wallet_detail_view(request):
    return render(request, 'billetera/mi_billetera.html')


