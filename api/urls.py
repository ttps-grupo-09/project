import api.publicaciones.views as publicacion_views
import api.productos.views as productos_views
import api.compras.views as compras_views
import api.calificaciones.views as calificaciones_views
import api.billetera.views as billetera_views


from django.urls import path

urlpatterns = [
    path('publicaciones/vendedor/', publicacion_views.publicaciones_vendedor_view, name="api-publicaciones-vendedor"),
    path('publicaciones/create/', publicacion_views.create_view, name="api-publicaciones-create"),
    path('publicaciones/update/<int:pk>/', publicacion_views.update_view, name="api-publicaciones-update"),
    path('publicaciones/detail/<int:pk>/', publicacion_views.detail_view, name="api-publicaciones-detail"),
    path('publicaciones/incrementar_precios/', publicacion_views.publicaciones_incrementar_precios_view, name="api-publicaciones-incrementar-precios"),
    path('publicaciones/decrementar_precios/', publicacion_views.publicaciones_decrementar_precios_view, name="api-publicaciones-decrementar-precios"),
    path('productos/vendedor/', productos_views.productos_vendedor_view, name="api-productos-vendedor"),
    path('productos/productos_por_categoria/', productos_views.productos_por_categoria, name="api-productos-por-categoria"),
    path('compras/items/', compras_views.list_items_view, name="api-compras-items-list"),
    path('compras/items/buy/', compras_views.buy_items_view, name="api-compras-items-buy"),
    path('compras/items/buyed/', compras_views.get_comprador_transacciones_view, name="api-compras-items-buyed"),
    path('compras/items/sold/', compras_views.get_vendedor_transacciones_view, name="api-compras-items-sold"),
    path('compras/items/add/<int:pk>/', compras_views.add_item_view, name="api-compras-items-add"),
    path('compras/items/delete/<int:pk>/', compras_views.delete_item_view, name="api-compras-items-delete"),
    path('compras/items/update_cantidad/<int:pk>/', compras_views.update_cantidad_item, name="api-compras-items-update-cantidad"),
    path('compras/estadisticas/publicaciones', compras_views.get_estadisticas_publicacion_view, name="api-compras-estadisticas-publicaciones"),
    path('calificaciones/<int:pk>/', calificaciones_views.calificaciones_list_view, name="api-calificaciones-list"),
    path('calificaciones/create/<int:pk>/', calificaciones_views.calificaciones_create_view, name="api-calificaciones-create"),
    path('billetera/movimientos/', billetera_views.billetera_list_view, name="api-billetera-movimientos-list"),
    path('billetera/cargar_saldo/', billetera_views.billetera_cargar_saldo_view, name="api-billetera-cargar-saldo"),
    path('billetera/retirar_saldo/', billetera_views.billetera_retirar_saldo_view, name="api-billetera-retirar-saldo"),
]