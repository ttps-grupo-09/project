# http://yceruto.github.io/django-ajax/
from django_ajax.decorators import ajax
from django.http import JsonResponse
from django.contrib.auth.decorators import login_required
from publicaciones.models import Publicacion
from compras.models import Transaccion
from billetera.models import *

def throw_error(error):
    response = JsonResponse({'error': error})
    response.status_code = 400
    return response

@ajax
@login_required
def buy_items_view(request):
    user = request.user
    for item in user.carrito.get_carrito_items():
        t = Transaccion.create_transaccion(
            item_carrito=item
        )
        # Decrementar stock de publicacion según cantidad de items.
        t.publicacion.decrementar_stock(t.cantidad)
        
        # Retirar creditos del comprador
        t.get_comprador().billetera.add_item({
            'tipo': ItemBilletera.COMPRA,
            'descripcion': t.publicacion.nombre,
            'monto': t.get_precio_total()
        })
        
        # Depositar creditos en el vendedor
        t.get_vendedor().billetera.add_item({
            'tipo': ItemBilletera.VENTA,
            'descripcion': t.publicacion.nombre,
            'monto': t.get_precio_total()
        })

        # TODO: Depositar comision de Buylink
        
        # Incrementar Puntos al comprador
        t.get_comprador().billetera.incrementar_puntos(
            monto_gastado=t.get_precio_total()
        )
    user.carrito.vaciar_carrito()
    return {'response': 'Ha adquirido exitosamente los productos!'}

@ajax
@login_required
def list_items_view(request):
    items = []
    for item in request.user.carrito.get_carrito_items():
        items.append(item.serialize())
    return {'items': items}

@login_required
def delete_item_view(request, pk):
    try:
        publicacion = Publicacion.objects.get(pk=pk)
    except Publicacion.DoesNotExist:
        response = JsonResponse({'error': 'La publicacion no existe'})
        response.status_code = 400
        return response
    request.user.carrito.remove_item(
        publicacion=publicacion
    )
    return JsonResponse({'response': 'deleted'})

@ajax
@login_required
def add_item_view(request, pk):
    user = request.user
    try:
        publicacion = Publicacion.objects.get(pk=pk)
    except Publicacion.DoesNotExist:
        response = JsonResponse({'error': 'La publicacion no existe'})
        response.status_code = 400
        return response
    cantidad = int(request.GET.get('cantidad'))
    if not publicacion.cantidad_en_stock(cantidad):
        return throw_error('La cantidad Ingresada supera el stock')
    if user.carrito.has_item(publicacion):
        item = user.carrito.get_item(publicacion)
        if item.supera_stock(cantidad):
            return throw_error('Tu carrito ya tiene este producto. Superas el stock')
    user.carrito.add_item(
        publicacion=publicacion,
        cantidad=cantidad
    )
    return JsonResponse({'response': 'added'})

@ajax
@login_required
def update_cantidad_item(request, pk):
    user = request.user
    try:
        publicacion = Publicacion.objects.get(pk=pk)
    except Publicacion.DoesNotExist:
        return throw_error('La publicacion no existe')
    cantidad = int(request.GET.get('cantidad'))
    user.carrito.update_cantidad_item(
        publicacion=publicacion,
        cantidad=cantidad
    )
    return JsonResponse({'response': 'updated'})

@ajax
@login_required
def get_comprador_transacciones_view(request):
    user = request.user
    transacciones = []
    for t in user.comprasmanager.get_compras():
        transacciones.append(t.serialize())
    return {
        'transacciones': transacciones
    }

@ajax
@login_required
def get_vendedor_transacciones_view(request):
    user = request.user
    transacciones = []
    for t in user.ventasmanager.get_ventas():
        transacciones.append(t.serialize())
    return {
        'transacciones': transacciones
    }

@ajax
@login_required
def get_estadisticas_publicacion_view(request):
    user = request.user
    publicaciones = []
    for p in user.publicacion_set.all():
        publicaciones.append({
            'nombre': p.nombre,
            'ventas': p.get_cantidad_ventas_publicacion(),
            'monto_acumulado': p.get_monto_acumulado_publicacion()
        })
    return {
        'publicaciones': publicaciones
    }