# http://yceruto.github.io/django-ajax/
import json
from django_ajax.decorators import ajax
from usuarios.models import User
from productos.models import Categoria, Producto
from publicaciones.models import Publicacion, Imagen
from django.views.decorators.csrf import csrf_exempt
from django.contrib.auth.decorators import login_required
from django.http import JsonResponse

@ajax
def detail_view(request, pk):
    try:
        publicacion = Publicacion.objects.get(pk=pk)
    except Publicacion.DoesNotExist:
        return {'status': 400}
    return {'publicacion': publicacion.serialize()}

@ajax
def publicaciones_vendedor_view(request):
    publicaciones = []
    for p in Publicacion.objects.filter(user=request.user):
        serialized_repr = {
            'nombre': p.nombre
        }
        publicaciones.append(serialized_repr)
    return {
        'publicaciones': publicaciones
    }

@csrf_exempt
@ajax
def create_view(request):
    if request.method == 'POST':
        data = json.loads(request.body)
        try:
            producto = Producto.objects.get(id=data.get('producto_id'))
        except Producto.DoesNotExist:
            return {'status': 400}
        p = Publicacion.objects.create(
            user=request.user,
            producto=producto,
            nombre = data.get('nombre'),
            detalle = data.get('detalle'),
            precio_base = data.get('precio_base'),
            precio_minimo = data.get('precio_minimo'),
            precio_maximo = data.get('precio_maximo'),
            stock = data.get('stock'),
            estado = data.get('estado')
        )
        for img in data.get('images'):
            Imagen.objects.create(publicacion=p, url=img)
    
    return {'result': 'created'}

@csrf_exempt
@ajax
def update_view(request, pk):
    if request.method == 'POST':
        data = json.loads(request.body)
        try:
            producto = Producto.objects.get(id=data.get('producto_id'))
        except Producto.DoesNotExist:
            return {'status': 400}
        try:
            p = Publicacion.objects.get(pk=pk)
        except Publicacion.DoesNotExist:
            return {'status': 400}

        p.producto=producto
        p.nombre = data.get('nombre')
        p.detalle = data.get('detalle')
        p.precio_base = data.get('precio_base')
        p.precio_minimo = data.get('precio_minimo')
        p.precio_maximo = data.get('precio_maximo')
        p.stock = data.get('stock')
        p.estado = data.get('estado')
        p.save()

        p.imagen_set.all().delete()
        for img in data.get('images'):
            Imagen.objects.create(publicacion=p, url=img)
    
    return {'result': 'created'}

@login_required
def publicaciones_incrementar_precios_view(request):
    try:  
        percentage = float(request.GET.get('percentage'))
    except Exception:
        response = JsonResponse({'error': 'Se requiere un número.'})
        response.status_code = 400
        return response
    if percentage <= 0:
        response = JsonResponse({'error': 'Se requiere un porcentaje mayor a 0'})
        response.status_code = 400
        return response
    Publicacion.Incrementar_porcentualmente_precios(
        queryset=Publicacion.objects.filter(user=request.user),
        percentage=float(request.GET.get('percentage'))
    )
    return JsonResponse({'result': 'modified'})

@login_required
def publicaciones_decrementar_precios_view(request):
    try:  
        percentage = float(request.GET.get('percentage'))
    except Exception:
        response = JsonResponse({'error': 'Se requiere un número.'})
        response.status_code = 400
        return response
    if percentage <= 0:
        response = JsonResponse({'error': 'Se requiere un porcentaje mayor a 0'})
        response.status_code = 400
        return response
    Publicacion.Decrementar_porcentualmente_precios(
        queryset=Publicacion.objects.filter(user=request.user),
        percentage=float(request.GET.get('percentage'))
    )
    return JsonResponse({'result': 'modified'})