# http://yceruto.github.io/django-ajax/
import json
from django_ajax.decorators import ajax
from django.http import JsonResponse
from django.contrib.auth.decorators import login_required
from django.views.decorators.csrf import csrf_exempt
from configuracion.models import Configuracion
from billetera.models import *

def throw_error(error):
    response = JsonResponse({'error': error})
    response.status_code = 400
    return response

@ajax
@login_required
def billetera_list_view(request):
    user = request.user
    movimientos = []
    for m in user.billetera.get_billetera_items():
        movimientos.append(m.serialize())
    return {
        'saldo': float("{0:.2f}".format(user.billetera.saldo)),
        'puntos': user.billetera.puntos,
        'movimientos': movimientos,
    }

@ajax
@csrf_exempt
@login_required
def billetera_cargar_saldo_view(request):
    monto = json.loads(request.body)
    user = request.user
    user.billetera.add_item({
        'tipo': ItemBilletera.CARGA,
        'descripcion': ItemBilletera.CARGA_DESCRIPCION,
        'monto': float(monto)
    })
    return {'response': 'Su saldo fue cargado exitosamente!'}

@ajax
@csrf_exempt
@login_required
def billetera_retirar_saldo_view(request):
    monto = json.loads(request.body)
    user = request.user
    user.billetera.add_item({
        'tipo': ItemBilletera.RETIRO,
        'descripcion': ItemBilletera.RETIRO_DESCRIPCION,
        'monto': float(monto)
    })
    return {'response': 'Su saldo fue extraido exitosamente!'}