# http://yceruto.github.io/django-ajax/
import json
from django_ajax.decorators import ajax
from django.http import JsonResponse
from django.contrib.auth.decorators import login_required
from publicaciones.models import Publicacion, Calificacion
from configuracion.models import Configuracion
from django.views.decorators.csrf import csrf_exempt

def throw_error(error):
    response = JsonResponse({'error': error})
    response.status_code = 400
    return response

@ajax
@login_required
def calificaciones_list_view(request, pk):
    try:
        publicacion = Publicacion.objects.get(pk=pk)
    except Publicacion.DoesNotExist:
        return throw_error('La Publicacion no existe')
    calificaciones = []
    config = Configuracion.Get_config()
    for c in publicacion.get_calificaciones()[:config.cantidad_calificaciones]:
        calificaciones.append(c.serialize())
    return { 
        'promedio_puntaje': publicacion.get_puntaje_promedio_calificaciones(),
        'calificaciones': calificaciones,
    }

@ajax
@csrf_exempt
@login_required
def calificaciones_create_view(request, pk):
    try:
        publicacion = Publicacion.objects.get(pk=pk)
    except Publicacion.DoesNotExist:
        return throw_error('La Publicacion no existe')
    data = json.loads(request.body)
    publicacion.add_calificacion(request.user, data)
    return {'response': 'La calificación ha sido creada exitosamente!'}
    