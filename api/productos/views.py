# http://yceruto.github.io/django-ajax/
from django_ajax.decorators import ajax
from productos.models import Categoria, Producto

@ajax
def productos_por_categoria(request):
    categorias = []
    for c in Categoria.objects.all():
        serialized_repr = {
            'id': c.id,
            'label': c.nombre,
            'children': [producto.dict_repr() for producto in c.producto_set.all()]
        }
        categorias.append(serialized_repr)

    return {'categorias': categorias}

@ajax
def productos_vendedor_view(request):
    productos = []
    for p in Producto.objects.filter(publicacion__user=request.user).distinct():
        serialized_repr = {
            'id': p.id,
            'nombre': p.nombre
        }
        productos.append(serialized_repr)

    return {'productos': productos}