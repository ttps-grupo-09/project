# Generated by Django 2.2 on 2019-12-18 14:41

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('configuracion', '0003_configuracion_percentage'),
    ]

    operations = [
        migrations.AddField(
            model_name='configuracion',
            name='balance_plataforma',
            field=models.IntegerField(default=0),
        ),
    ]
