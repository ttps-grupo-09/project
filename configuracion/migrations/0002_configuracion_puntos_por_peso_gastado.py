# Generated by Django 2.2 on 2019-12-18 13:27

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('configuracion', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='configuracion',
            name='puntos_por_peso_gastado',
            field=models.IntegerField(default=1),
        ),
    ]
