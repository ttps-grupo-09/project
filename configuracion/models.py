from django.db import models

class Configuracion(models.Model):
    balance_plataforma = models.IntegerField(default=0)
    cantidad_calificaciones = models.IntegerField(default=5)
    puntos_por_peso_gastado = models.IntegerField(default=1)
    percentage = models.IntegerField(default=5)

    @classmethod
    def Get_config(cls):
        return cls.objects.first()
    
    def get_puntos_por_peso(self, monto_gastado):
        return int(monto_gastado * self.puntos_por_peso_gastado)
    
    def descontar_comision_plataforma(self, monto):
        self.balance_plataforma = self.percentage * monto / 100
        self.save()
        return monto - (self.percentage * monto / 100)