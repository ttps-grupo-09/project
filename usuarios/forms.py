from django import forms
from django.contrib.auth.forms import UserCreationForm
from usuarios.models import User

class CustomUserCreationForm(UserCreationForm):
    email = forms.EmailField(
        widget=forms.EmailInput(
            attrs={
                'placeholder': 'Email'
            }
        )
    )
    nombre = forms.CharField(
        max_length=150,
        widget=forms.TextInput(
            attrs={
                'placeholder': 'Nombre'
            }
        )
    )
    apellido = forms.CharField(
        max_length=150,
        widget=forms.TextInput(
            attrs={
                'placeholder': 'Apellido'
            }
        )
    )
    fecha_nacimiento = forms.DateTimeField(
        widget=forms.DateTimeInput(
            attrs={
                'type': 'date',
                'placeholder': 'Fecha de Nacimiento'
            }
        )
    )
    password1 = forms.CharField(
        widget=forms.PasswordInput(
            attrs={
                'placeholder': 'Contraseña'
            }
        )
    )
    password2 = forms.CharField(
        widget=forms.PasswordInput(
            attrs={
                'placeholder': 'Contraseña'
            }
        )
    )

    class Meta:
        model = User
        fields = ('email', 'nombre', 'apellido', 'fecha_nacimiento', 'password1', 'password2')
    
    def __init__(self, *args, **kwargs):
        super(CustomUserCreationForm, self).__init__(*args, **kwargs)
        self.fields['password1'].label = "Contraseña"
        self.fields['password2'].label = "Confirme su Contraseña"