from django.utils import timezone
from django.db import models
from django.contrib.auth.models import (
    AbstractBaseUser,
    PermissionsMixin
)
from django.contrib.auth.base_user import BaseUserManager

class UserManager(BaseUserManager):
    use_in_migrations = True

    def _create_user(self, email, password, **extra_fields):
        """
        Creates and saves a User with the given email and password.
        """
        if not email:
            raise ValueError('The given email must be set')
        email = self.normalize_email(email)
        user = self.model(email=email, **extra_fields)
        user.set_password(password)
        user.save(using=self._db)
        return user

    def create_user(self, email, password=None, **extra_fields):
        extra_fields.setdefault('is_superuser', False)
        return self._create_user(email, password, **extra_fields)

    def create_superuser(self, email, password, **extra_fields):
        extra_fields.setdefault('is_superuser', True)

        if extra_fields.get('is_superuser') is not True:
            raise ValueError('Superuser must have is_superuser=True.')

        return self._create_user(email, password, **extra_fields)
    
    def get_by_natural_key(self, username):
        case_insensitive_username_field = '{}__iexact'.format(self.model.USERNAME_FIELD)
        return self.get(**{case_insensitive_username_field: username})

class User(AbstractBaseUser, PermissionsMixin):
    email = models.EmailField(unique=True)
    nombre = models.CharField(max_length=150, null=True)
    apellido = models.CharField(max_length=150, null=True)
    fecha_nacimiento = models.DateTimeField(null=True)
    date_joined = models.DateTimeField(default=timezone.now)
    is_active = models.BooleanField(default=True)
    is_superuser = models.BooleanField(default=False)
    is_staff = models.BooleanField(default=False)

    objects = UserManager()

    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = []
    class Meta:
        verbose_name = 'user'
        verbose_name_plural = 'users'
    
    def get_full_name(self):
        return f"{self.nombre} {self.apellido}"

    def asignar_relateds(self):
        from compras.models import Carrito, VentasManager, ComprasManager
        from billetera.models import Billetera

        Billetera.objects.create(user=self)
        Carrito.objects.create(user=self)
        VentasManager.objects.create(vendedor=self)
        ComprasManager.objects.create(comprador=self)
    
    def get_tipo_precio_actual(self):
        """
        Devuelve el tipo de precio actual utilizado
        por el usuario.
        """
        from publicaciones.models import Publicacion

        if self.publicacion_set.exists():
            return self.publicacion_set.first().tipo_precio_actual
        else:
            return Publicacion.PRECIO_BASE
    
    def serialize(self):
        return {
            'id': self.pk,
            'email': self.email,
            'nombre_completo': self.get_full_name(),
        }
    
