from django.conf import settings

def base_domain(request):
    return {'BASE_DOMAIN': settings.BASE_DOMAIN}