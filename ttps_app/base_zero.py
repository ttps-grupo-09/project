from django.utils import timezone
from publicaciones.models import *
from productos.models import *
from usuarios.models import User
from billetera.models import *
from configuracion.models import Configuracion

Configuracion.objects.all().delete()
Configuracion.objects.create()

users = [
    {
        'nombre': 'Juan',
        'apellido': 'comprador1',
        'email': 'juan_comprador@mail.com',
        'password': '1234',
        'fecha_nacimiento': timezone.now()
    },
    {
        'nombre': 'Maria',
        'apellido': 'comprador2',
        'email': 'maria_comprador@mail.com',
        'password': '1234',
        'fecha_nacimiento': timezone.now()
    },
    {
        'nombre': 'Josefina',
        'apellido': 'vendedor1',
        'email': 'josefina_vendedor@mail.com',
        'password': '1234',
        'fecha_nacimiento': timezone.now()
    },
    {
        'nombre': 'Julian',
        'apellido': 'vendedor2',
        'email': 'julian_vendedor@mail.com',
        'password': '1234',
        'fecha_nacimiento': timezone.now()
    }
]

User.objects.all().delete()
for u in users:
    user = User.objects.create_user(email=u.get('email'), password=u.get('password'))
    user.nombre = u.get('nombre')
    user.apellido = u.get('apellido')
    user.save()
    user.asignar_relateds()

categorias = [
    {'codigo': 'MLA1000', 'nombre':'Electrónica, Audio y Video'},
    {'codigo': 'MLA1039', 'nombre':'Cámaras y Accesorios'},
    {'codigo': 'MLA1051', 'nombre':'Celulares y Teléfonos'},
    {'codigo': 'MLA1071', 'nombre':'Animales y Mascotas'},
    {'codigo': 'MLA1132', 'nombre':'Juegos y Juguetes'},
    {'codigo': 'MLA1144', 'nombre':'Consolas y Videojuegos'},
    {'codigo': 'MLA1168', 'nombre':'Música, Películas y Series'},
    {'codigo': 'MLA1182', 'nombre':'Instrumentos Musicales'},
    {'codigo': 'MLA1246', 'nombre':'Belleza y Cuidado Personal'},
    {'codigo': 'MLA1276', 'nombre':'Deportes y Fitness'},
    {'codigo': 'MLA1367', 'nombre':'Antigüedades y Colecciones'},
    {'codigo': 'MLA1368', 'nombre':'Arte, Librería y Mercería'},
    {'codigo': 'MLA1384', 'nombre':'Bebés'},
    {'codigo': 'MLA1403', 'nombre':'Alimentos y Bebidas'},
    {'codigo': 'MLA1430', 'nombre':'Ropa y Accesorios'},
    {'codigo': 'MLA1459', 'nombre':'Inmuebles'},
    {'codigo': 'MLA1499', 'nombre':'Industrias y Oficinas'},
    {'codigo': 'MLA1540', 'nombre':'Servicios'},
    {'codigo': 'MLA1574', 'nombre':'Hogar, Muebles y Jardín'},
    {'codigo': 'MLA1648', 'nombre':'Computación'},
    {'codigo': 'MLA1743', 'nombre':'Autos, Motos y Otros'},
    {'codigo': 'MLA1953', 'nombre':'Otras categorías'},
    {'codigo': 'MLA2547', 'nombre':'Entradas para Eventos'},
    {'codigo': 'MLA3025', 'nombre':'Libros Revistas y Comics'},
    {'codigo': 'MLA3937', 'nombre':'Joyas y Relojes'},
    {'codigo': 'MLA407134', 'nombre':'Herramientas y Construcción'},
    {'codigo': 'MLA409431', 'nombre':'Salud y Equipamiento Médico'},
    {'codigo': 'MLA5725', 'nombre':'Accesorios para Vehículos'},
    {'codigo': 'MLA5726', 'nombre':'Electrodomésticos y Aires Ac.'},
    {'codigo': 'MLA9304', 'nombre':'Souvenirs, Cotillón y Fiestas'}
]

Categoria.objects.all().delete()
for c in categorias:
    Categoria.objects.create(
        codigo=c.get("codigo"),
        nombre=c.get("nombre")
    )

productos = [
    {
        'nombre': "Memoria Micro Sd Hc 16 Gb Kingston Clase 10 Tienda",
        'descripcion': "Siendo la tarjeta SD más pequeña disponible, la microSD Clase 10 es la opción de almacenamiento expandible estándar para muchas tablets, teléfonos inteligentes y cámaras de acción.",
        'categoria': 'MLA1051',
        'marca': 'Kingston'
    },
    {
        'nombre': "Celular samsung j6 plus, linea galaxy J",
        'descripcion': "Modelo j6 posee memoria interna de 32 gb uno de los mejores celulares de gama media,pantalla vibrante infinity display.",
        'categoria': 'MLA1051',
        'marca': 'Samsung'
    },
    {
        'nombre': "Perros Cachorros Caniche Toy",
        'descripcion': "Cachorritos caniche toy., a partir de los 45 días para la venta. ",
        'categoria': 'MLA1071',
        'marca': ''
    },
    {
        'nombre': "Piano cola steinway essex egp 155 negro nuevo",
        'descripcion': "Piano de cola steinway essex. Modelo nuevo 155 egp.",
        'categoria': 'MLA1182',
        'marca': 'Steinway'
    },
    {
        'nombre': "Violonchelo  cello 4/4 custom parquer ce900",
        'descripcion': "Violonchelos de diferente tipo",
        'categoria': 'MLA1182',
        'marca': 'Parquer'
    },
    {
        'nombre': "Cerveza  porroncito 340 ml envase no retornable",
        'descripcion': "Cervezas de todo tipo",
        'categoria': 'MLA1403',
        'marca': 'Quilmes'
    },
    {
        'nombre': "Smart TV Samsung 32” UN32J4290AGXZD",
        'descripcion': "Sumergite en la pantalla Con el Smart TV Samsung UN32J4290, viví una nueva experiencia visual con la resolución HD, que te presentará imágenes con gran detalle y de alta calidad. Ahora todo lo que veas cobrará vida con brillo y colores más reales. Gracias a su tamaño de 32 pulgadas, podrás transformar tu espacio en una sala de cine y transportarte a donde quieras.",
        'categoria': 'MLA1000',
        'marca': 'Samsung'
    },
]

Producto.objects.all().delete()
for p in productos:
    prod = Producto()
    prod.nombre = p.get("nombre")
    prod.descripcion = p.get("descripcion")
    prod.marca = p.get("marca")
    prod.modelo = ''
    prod.save()
    prod.categoria.add(Categoria.objects.get(codigo=p.get("categoria")))

publicaciones = [
    {
        'user': User.objects.get(nombre='Josefina'),
        'nombre': 'Celular 100% original,sellado,ensamblado en tierra del fuego. Varios colores, poseeo mucho stock',
        'detalle': 'excelente estado etc',
        'estado': Publicacion.ACTIVA,
        'tipo_precio_actual': Publicacion.PRECIO_BASE,
        'precio_base': 10000,
        'precio_minimo': 8500,
        'precio_maximo': 15000,
        'stock': 24,
        'imagen': 'https://images.mobilefun.co.uk/graphics/450pixelp/69875.jpg',
        'nombre_prod': 'Celular samsung j6 plus, linea galaxy J'
    },
    {
        'user': User.objects.get(nombre='Josefina'),
        'nombre': 'celular condicionado, ningun detalle ensamblado en estados unidos. Compra hoy de regalo una micro sd 32 gb',
        'detalle': '.buen estado',
        'estado': Publicacion.SIN_STOCK,
        'tipo_precio_actual': Publicacion.PRECIO_MAXIMO,
        'precio_base': 8000,
        'precio_minimo': 6500,
        'precio_maximo': 12000,
        'stock': 0,
        'imagen': 'https://images.mobilefun.co.uk/graphics/450pixelp/69875.jpg',
        'nombre_prod': 'Celular samsung j6 plus, linea galaxy J'
    },
    {
        'user': User.objects.get(nombre='Julian'),
        'nombre': 'Desparasitados,vacunados y listo para retirar',
        'detalle': 'Todos muy mimosos son',
        'estado': Publicacion.ACTIVA,
        'tipo_precio_actual': Publicacion.PRECIO_MINIMO,
        'precio_base': 9200,
        'precio_minimo': 9000,
        'precio_maximo': 9500,
        'stock': 9,
        'imagen': 'https://www.salvaterrademagos.es/wp-content/uploads/2018/02/Sweet-de-Salvaterra-de-Magos-con-la-familia-Raventos-en-Sant-Sadurni-D%C2%B4anoia-e1521187851801.png',
        'nombre_prod': 'Perros Cachorros Caniche Toy'
    },
    {
        'user': User.objects.get(nombre='Josefina'),
        'nombre': 'Violonchelo excelente estado año 2006 tenemos stock. Estilo clasico con microafinador',
        'detalle': '...',
        'estado': Publicacion.INACTIVA,
        'tipo_precio_actual': Publicacion.PRECIO_BASE,
        'precio_base': 50000,
        'precio_minimo': 48000,
        'precio_maximo': 52000,
        'stock': 1,
        'imagen': 'https://images-na.ssl-images-amazon.com/images/I/41SlqFCl0AL._SX466_.jpg',
        'nombre_prod': 'Violonchelo  cello 4/4 custom parquer ce900'
    },
    {
        'user': User.objects.get(nombre='Josefina'),
        'nombre': 'Piano  Ideal Estudio ¼ de cola de contado hacemos un 5% de descuento,tengo stock tambien en acabado marron',
        'detalle': '...',
        'estado': Publicacion.ACTIVA,
        'tipo_precio_actual': Publicacion.PRECIO_MAXIMO,
        'precio_base': 15100,
        'precio_minimo': 9500,
        'precio_maximo': 18100,
        'stock': 1,
        'imagen': 'http://laguiadelpiano.com/wp-content/uploads/2015/03/stenway-D-gran-cola1.jpg',
        'nombre_prod': 'Piano cola steinway essex egp 155 negro nuevo'
    },
    {
        'user': User.objects.get(nombre='Julian'),
        'nombre': 'quilmes cristal fria  hasta las 4 am. Precio de costo hasta las 22:00',
        'detalle': '...con pizza...',
        'estado': Publicacion.ACTIVA,
        'tipo_precio_actual': Publicacion.PRECIO_MINIMO,
        'precio_base': 220,
        'precio_minimo': 220,
        'precio_maximo': 260,
        'stock': 125,
        'imagen': 'http://decervecitas.com/362-large_default/quilmes-cristal-34-cl.jpg',
        'nombre_prod': 'Cerveza  porroncito 340 ml envase no retornable'
    },
    {
        'user': User.objects.get(nombre='Josefina'),
        'nombre': 'Quilmes Bock',
        'detalle': '..muy buena..',
        'estado': Publicacion.ACTIVA,
        'tipo_precio_actual': Publicacion.PRECIO_BASE,
        'precio_base': 250,
        'precio_minimo': 250,
        'precio_maximo': 310,
        'stock': 63,
        'imagen': 'https://boulevard-sa.com.ar/Site/img/products/quilmes/Quilmes-bock-340-L.jpg',
        'nombre_prod': 'Cerveza  porroncito 340 ml envase no retornable'
    },
    {
        'user': User.objects.get(nombre='Julian'),
        'nombre': 'Imperial Stout',
        'detalle': 'ideal para el sabado!',
        'estado': Publicacion.INACTIVA,
        'tipo_precio_actual': Publicacion.PRECIO_BASE,
        'precio_base': 280,
        'precio_minimo': 280,
        'precio_maximo': 345,
        'stock': 45,
        'imagen': 'http://d26lpennugtm8s.cloudfront.net/stores/447/761/products/imperial-cream-stout-porron-500cc1-05e29b29550c530e4015670305315592-640-0.jpg',
        'nombre_prod': 'Cerveza  porroncito 340 ml envase no retornable'
    },
    {
        'user': User.objects.get(nombre='Julian'),
        'nombre': 'Cerveza Andes roja artesanal bien dulce',
        'detalle': '..rica..',
        'estado': Publicacion.SIN_STOCK,
        'tipo_precio_actual': Publicacion.PRECIO_MAXIMO,
        'precio_base': 300,
        'precio_minimo': 350,
        'precio_maximo': 400,
        'stock': 0,
        'imagen': 'https://statics.dinoonline.com.ar/imagenes/large_460x460/3100628_l.jpg',
        'nombre_prod': 'Cerveza  porroncito 340 ml envase no retornable'
    },
    {
        'user': User.objects.get(nombre='Josefina'),
        'nombre': 'Sumergite en la pantalla. Comprando el televisor tenes un 5% de descuento si lo compras en los últimos días del mes y de contando te llevas de regalo un decodificador',
        'detalle': 'Con el Smart TV Samsung UN50MU6100, viví una nueva experiencia visual con la resolución 4K, que te presentará imágenes con gran detalle y de alta calidad',
        'estado': Publicacion.ACTIVA,
        'tipo_precio_actual': Publicacion.PRECIO_MINIMO,
        'precio_base': 39900,
        'precio_minimo': 28000,
        'precio_maximo': 65000,
        'stock': 10,
        'imagen': 'https://mla-s1-p.mlstatic.com/874188-MLA31659174651_082019-F.webp',
        'nombre_prod': 'Smart TV Samsung 32” UN32J4290AGXZD'
    }
]

Publicacion.objects.all().delete()
Imagen.objects.all().delete()
for p in publicaciones:
    pub = Publicacion()
    pub.user = p.get('user')
    pub.producto = Producto.objects.get(nombre=p.get('nombre_prod'))
    pub.nombre = p.get('nombre')
    pub.detalle = p.get('detalle')
    # pub.tipo_precio_actual = p.get('tipo_precio_actual')
    pub.precio_base = p.get('precio_base')
    pub.precio_minimo = p.get('precio_minimo')
    pub.precio_maximo = p.get('precio_maximo')
    pub.stock = p.get('stock')
    pub.imagen = p.get('imagen')
    pub.estado = p.get('estado')
    pub.save()
    Imagen.objects.create(publicacion=pub, url=p.get('imagen'))

juan = User.objects.get(nombre="Juan")
juan.carrito.add_item(
    publicacion=Publicacion.objects.get(nombre="Cerveza Andes roja artesanal bien dulce"),
    cantidad=6
)
juan.carrito.add_item(
    publicacion=Publicacion.objects.get(nombre="Imperial Stout"),
    cantidad=10
)
juan.carrito.add_item(
    publicacion=Publicacion.objects.get(nombre="Quilmes Bock"),
    cantidad=65
)
juan.carrito.add_item(
    publicacion=Publicacion.objects.get(nombre="quilmes cristal fria  hasta las 4 am. Precio de costo hasta las 22:00"),
    cantidad=15
)


#josefina = User.objects.get(nombre="Josefina")
#josefina.billetera.add_item(
#    publicacion=Publicacion.objects.get(nombre="quilmes cristal fria  hasta las 4 am. Precio de costo hasta las 22:00"),
#    cantidad=15
#)
#
#josefina.billetera.add_item(
#    publicacion=Publicacion.objects.get(nombre="Quilmes Bock"),
#    cantidad=65
#)