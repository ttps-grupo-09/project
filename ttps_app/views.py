from django.shortcuts import render
from publicaciones.models import Publicacion
from django.views.generic import (ListView, 
                                DetailView)

class HomeListView(ListView):

    template_name = 'home.html'
    context_object_name = 'publicaciones'

    def get_queryset(self):
        return Publicacion.Publicaciones_con_mas_ventas()