import compras.views as views
from django.urls import path

urlpatterns = [
    path('', views.shopping_cart_detail_view, name="compras-detail"),
    path('mis_compras/', views.mis_compras_view, name="compras-mias"),
    path('mis_ventas/', views.mis_ventas_view, name="ventas-mias"),
    path('mis_estadisticas/', views.mis_estadisticas_view, name="estadisticas-mias"),
]