from django.shortcuts import render
from django.contrib.auth.decorators import login_required

@login_required
def shopping_cart_detail_view(request):
    return render(request, 'compras/index.html')

@login_required
def mis_compras_view(request):
    return render(request, 'compras/comprador_list.html')

@login_required
def mis_ventas_view(request):
    return render(request, 'compras/vendedor_list.html')

@login_required
def mis_estadisticas_view(request):
    return render(request, 'compras/estadisticas.html')