from django.db import models
from usuarios.models import User
from publicaciones.models import Publicacion

class Carrito(models.Model):
    user = models.OneToOneField(User, blank=True, null=True, on_delete=models.SET_NULL)

    def vaciar_carrito(self):
        for item in self.get_carrito_items():
            self.remove_item(item.publicacion)

    def get_carrito_items(self):
        return self.itemcarrito_set.all()
    
    def add_item(self, publicacion, cantidad):
        if self.itemcarrito_set.filter(publicacion=publicacion).exists():
            item = self.itemcarrito_set.get(publicacion=publicacion)
            item.cantidad += cantidad
            item.save()
        else:
            ItemCarrito.objects.create(
                carrito=self,
                publicacion=publicacion,
                cantidad=cantidad
            )
    
    def remove_item(self, publicacion):
        self.get_carrito_items().filter(publicacion=publicacion).delete()
    
    def has_item(self, publicacion):
        return self.get_carrito_items().filter(publicacion=publicacion).exists()

    def get_item(self, publicacion):
        return self.get_carrito_items().get(publicacion=publicacion)
    
    def update_cantidad_item(self, publicacion, cantidad):
        if self.has_item(publicacion):
            item = self.get_item(publicacion)
            item.cantidad = cantidad
            item.save()


class ItemCarrito(models.Model):
    carrito = models.ForeignKey(Carrito, blank=True, null=True, on_delete=models.SET_NULL)
    publicacion = models.ForeignKey(Publicacion, blank=True, null=True, on_delete=models.SET_NULL)
    cantidad = models.IntegerField(default=1)
    date_added = models.DateTimeField(auto_now_add=True)

    def serialize(self):
        return {
            'id': self.publicacion.id,
            'nombre': self.publicacion.nombre,
            'stock': self.publicacion.stock,
            'precio': self.publicacion.get_precio_actual(),
            'imagen': self.publicacion.get_thumbnail(),
            'cantidad': self.cantidad,
            'estado': self.publicacion.estado
        }
    
    def supera_stock(self, cantidad):
        cantidad = self.cantidad + cantidad
        return cantidad > self.publicacion.stock
    
    def get_vendedor(self):
        return self.publicacion.user
    
    def get_comprador(self):
        return self.carrito.user


class VentasManager(models.Model):
    vendedor = models.OneToOneField(User, blank=True, null=True, on_delete=models.SET_NULL)

    def get_ventas(self):
        return self.transaccion_set.all()


class ComprasManager(models.Model):
    comprador = models.OneToOneField(User, blank=True, null=True, on_delete=models.SET_NULL)

    def get_compras(self):
        return self.transaccion_set.all()


class Transaccion(models.Model):
    ventas_manager = models.ForeignKey(VentasManager, blank=True, null=True, on_delete=models.SET_NULL)
    compras_manager = models.ForeignKey(ComprasManager, blank=True, null=True, on_delete=models.SET_NULL)
    publicacion = models.ForeignKey(Publicacion, blank=True, null=True, on_delete=models.SET_NULL)
    cantidad = models.IntegerField(default=1)
    precio_unitario = models.FloatField(default=0)
    date_added = models.DateTimeField(auto_now_add=True)

    def create_transaccion(item_carrito):
        return Transaccion.objects.create(
            compras_manager=item_carrito.get_comprador().comprasmanager,
            ventas_manager=item_carrito.get_vendedor().ventasmanager,
            publicacion=item_carrito.publicacion,
            cantidad=item_carrito.cantidad,
            precio_unitario=item_carrito.publicacion.get_precio_actual()
        )
    
    def serialize(self):
        return {
            'id': self.pk,
            'fecha': self.date_added.date(),
            'vendedor': self.ventas_manager.vendedor.serialize(),
            'comprador': self.compras_manager.comprador.serialize(),
            'publicacion': self.publicacion.serialize(),
            'cantidad': self.cantidad,
            'precio_unitario': self.precio_unitario,
            'is_calificada': self.publicacion.is_calificada(self.compras_manager.comprador)
        }

    def get_comprador(self):
        return self.compras_manager.comprador
    
    def get_vendedor(self):
        return self.ventas_manager.vendedor

    def get_precio_total(self):
        return self.cantidad * self.precio_unitario