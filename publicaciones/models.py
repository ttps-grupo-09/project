import operator
from functools import reduce
from django.db import models
from django.db.models import F, Q, When, Case
from productos.models import Producto
from usuarios.models import User

class Publicacion(models.Model):
    ACTIVA = 'Activa'
    INACTIVA = 'Inactiva'
    SIN_STOCK = 'Sin Stock'
    ESTADOS_CHOICES = [
        (ACTIVA, 'Activa'),
        (INACTIVA, 'Inactiva'),
        (SIN_STOCK, 'Sin Stock')
    ]

    PRECIO_BASE = 'Base'
    PRECIO_MINIMO = 'Precio Minimo'
    PRECIO_MAXIMO = 'Precio Maximo'
    TIPO_PRECIOS_CHOICES = [
        (PRECIO_BASE, 'Base'),
        (PRECIO_MINIMO, 'Precio Minimo'),
        (PRECIO_MAXIMO, 'Precio Maximo'),
    ]
    user = models.ForeignKey(User, blank=True, null=True, on_delete=models.SET_NULL)
    producto = models.ForeignKey(Producto, blank=True, null=True, on_delete=models.SET_NULL)
    nombre = models.TextField(max_length=1000)
    detalle = models.TextField(max_length=4000)
    tipo_precio_actual = models.CharField(max_length=255, choices=TIPO_PRECIOS_CHOICES, default=PRECIO_BASE)
    precio_base = models.FloatField(default=0)
    precio_minimo = models.FloatField(default=0)
    precio_maximo = models.FloatField(default=0)
    stock = models.IntegerField(default=0)
    estado = models.CharField(max_length=255, choices=ESTADOS_CHOICES, default=ACTIVA)
    ventas = models.IntegerField(default=0) # Luego sera una foreign key
    date_added = models.DateTimeField(auto_now_add=True)

    @classmethod
    def Publicaciones_con_mas_ventas(cls):
        return Publicacion.objects.filter(
            estado=cls.ACTIVA
        ).order_by('-ventas')[:10]
    
    @classmethod
    def Search(cls, queryset, keywords):
        query = reduce(operator.and_, (cls._build_keyword_lookup(kw) for kw in keywords))
        return queryset.filter(query)
    
    @classmethod
    def annotate_precio(cls, queryset):
        return queryset.annotate(
            precio_actual=Case(
                When(tipo_precio_actual=Publicacion.PRECIO_BASE, then='precio_base'),
                When(tipo_precio_actual=Publicacion.PRECIO_MINIMO, then='precio_minimo'),
                When(tipo_precio_actual=Publicacion.PRECIO_MAXIMO, then='precio_maximo'),
                output_field=models.FloatField()
            )
        )
    
    @classmethod
    def Cambiar_tipo_de_precio(cls, queryset, tipo_precio):
        queryset.update(tipo_precio_actual=tipo_precio)

    @classmethod
    def Incrementar_porcentualmente_precios(cls, queryset, percentage):
        queryset.update(
            precio_base=F('precio_base') + (percentage * F('precio_base') / 100),
            precio_minimo=F('precio_minimo') + (percentage * F('precio_minimo') / 100),
            precio_maximo=F('precio_maximo') + (percentage * F('precio_maximo') / 100)
        )
    
    @classmethod
    def Decrementar_porcentualmente_precios(cls, queryset, percentage):
        queryset.update(
            precio_base=F('precio_base') - (percentage * F('precio_base') / 100),
            precio_minimo=F('precio_minimo') - (percentage * F('precio_minimo') / 100),
            precio_maximo=F('precio_maximo') - (percentage * F('precio_maximo') / 100)
        )
    
    @classmethod
    def Cambiar_tipo_precio(cls, queryset, nuevo_tipo_precio):
        queryset.update(
            tipo_precio_actual=nuevo_tipo_precio
        )

    @classmethod
    def _build_keyword_lookup(cls, keyword):
        return Q(nombre__icontains=keyword) | Q(detalle__icontains=keyword) | Q(producto__nombre__icontains=keyword) | Q(producto__descripcion__icontains=keyword)

    def get_precio_actual(self):
        """
        Retorna el precio que haya elegido
        el vendedor para sus productos.
        """
        if self.tipo_precio_actual == Publicacion.PRECIO_BASE:   
            return self.precio_base
        elif self.tipo_precio_actual == Publicacion.PRECIO_MINIMO:   
            return self.precio_minimo
        elif self.tipo_precio_actual == Publicacion.PRECIO_MAXIMO:   
            return self.precio_maximo
    
    def get_thumbnail(self):
        return self.imagen_set.first().url
    
    def serialize(self):
        return {
            'id': self.pk,
            'nombre': self.nombre,
            'detalle': self.detalle,
            'producto_id': self.producto.id,
            'images': list(map(lambda img: img.url,self.imagen_set.all())),
            'precio_base': self.precio_base,
            'precio_minimo': self.precio_minimo,
            'precio_maximo': self.precio_maximo,
            'stock': self.stock,
            'estado': self.estado
        }
    
    def cantidad_en_stock(self, cantidad):
        return self.stock >= cantidad
    
    def get_calificaciones(self):
        return self.calificacion_set.all()
    
    def get_puntaje_promedio_calificaciones(self):
        puntaje_total = 0
        total_calificaciones = self.get_calificaciones().count()
        for c in self.get_calificaciones():
            puntaje_total += c.puntaje
        return (puntaje_total / total_calificaciones) if total_calificaciones else 0
    
    def add_calificacion(self, user, data):
        Calificacion.objects.create(
            user=user,
            publicacion=self,
            titulo=data.get('titulo'),
            descripcion=data.get('descripcion'),
            puntaje=int(data.get('puntaje'))
        )
    
    def is_calificada(self, comprador):
        return self.get_calificaciones().filter(user=comprador).exists()
    
    def decrementar_stock(self, cantidad):
        self.stock -= cantidad
        if self.stock == 0:
            self.estado = self.SIN_STOCK
        self.save()
    
    def is_inactiva(self):
        return self.estado == self.INACTIVA or self.estado == self.SIN_STOCK
    
    def get_cantidad_ventas_publicacion(self):
        total = 0
        for t in self.transaccion_set.all():
            total += t.cantidad
        return total

    def get_monto_acumulado_publicacion(self):
        total = 0
        for t in self.transaccion_set.all():
            total += t.cantidad * t.precio_unitario
        return total

class Imagen(models.Model):
    publicacion = models.ForeignKey(Publicacion, blank=True, null=True, on_delete=models.SET_NULL)
    url = models.TextField(max_length=5000)
    date_added = models.DateTimeField(auto_now_add=True)

class Calificacion(models.Model):
    PUNTAJE_MAXIMO = 5

    publicacion = models.ForeignKey(Publicacion, blank=True, null=True, on_delete=models.SET_NULL)
    user = models.ForeignKey(User, blank=True, null=True, on_delete=models.SET_NULL)
    titulo = models.CharField(max_length=255)
    descripcion = models.TextField(max_length=500)
    puntaje = models.IntegerField(default=PUNTAJE_MAXIMO)

    def serialize(self):
        return {
            'id': self.pk,
            'user_name': self.user.get_full_name(),
            'titulo': self.titulo,
            'descripcion': self.descripcion,
            'puntaje': self.puntaje
        }