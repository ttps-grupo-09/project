from django.shortcuts import render, redirect
from django.core.paginator import Paginator
from publicaciones.models import Publicacion
from productos.models import Categoria
from django.views.generic import (ListView, 
                                DetailView)
from publicaciones.filters import PublicacionFilter
from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.auth.decorators import login_required

class PublicacionesListView(ListView):
    def get_queryset(self):
        return Publicacion.objects.none()

    def get_paginated_results(self):
        paginator = Paginator(self.get_queryset(), 6)
        page = self.request.GET.get('page')
        return paginator.get_page(page)
    
    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['publicaciones'] = self.get_paginated_results()
        context = self.get_more_context_data(context)
        return context
    
    def get_more_context_data(self, context):
        """
        Hook for adding additional context
        """
        return context

class PublicacionesCompradorListView(PublicacionesListView):
    template_name = 'publicaciones/list.html'

    def get_queryset(self):
        return PublicacionFilter(
            self.request.GET, 
            Publicacion.objects.filter(estado=Publicacion.ACTIVA)
        ).qs
    
    def get_more_context_data(self, context):
        context['categorias'] = Categoria.objects.all()
        return context

class PublicacionesVendedorListView(LoginRequiredMixin, PublicacionesListView):
    template_name = 'publicaciones/vendedor_list.html'

    def get_queryset(self):
        return PublicacionFilter(
            self.request.GET,
            Publicacion.objects.filter(user=self.request.user)
        ).qs
    
    def get_more_context_data(self, context):
        context['tipo_precio_actual'] = self.request.user.get_tipo_precio_actual()
        context['constants'] = {
            'precio_base': Publicacion.PRECIO_BASE,
            'precio_minimo': Publicacion.PRECIO_MINIMO,
            'precio_maximo': Publicacion.PRECIO_MAXIMO,
        }
        return context

class PublicacionesDetailView(DetailView):
    """
    TODO: cambiar "similares"
    """
    queryset = Publicacion.objects.all()
    template_name = 'publicaciones/detail.html'
    context_object_name = 'publicacion'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['similares'] = Publicacion.Publicaciones_con_mas_ventas()
        return context

@login_required
def publicaciones_create_view(request):
    return render(request, 'publicaciones/create.html')

@login_required
def publicaciones_update_view(request, pk):
    context={
        'update': True,
        'publicacion': Publicacion.objects.get(pk=pk)
    }
    return render(request, 'publicaciones/create.html', context=context)

@login_required
def publicaciones_precio_base(request):
    Publicacion.Cambiar_tipo_precio(
        queryset=Publicacion.objects.filter(user=request.user),
        nuevo_tipo_precio=Publicacion.PRECIO_BASE
    )
    return redirect('publicaciones-vendedor-list')

@login_required
def publicaciones_precio_minimo(request):
    Publicacion.Cambiar_tipo_precio(
        queryset=Publicacion.objects.filter(user=request.user),
        nuevo_tipo_precio=Publicacion.PRECIO_MINIMO
    )
    return redirect('publicaciones-vendedor-list')

@login_required
def publicaciones_precio_maximo(request):
    Publicacion.Cambiar_tipo_precio(
        queryset=Publicacion.objects.filter(user=request.user),
        nuevo_tipo_precio=Publicacion.PRECIO_MAXIMO
    )
    return redirect('publicaciones-vendedor-list')