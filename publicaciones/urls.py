import publicaciones.views as views

from django.contrib import admin
from django.urls import path

urlpatterns = [
    path('', views.PublicacionesCompradorListView.as_view(), name="publicaciones-list"),
    path('<int:pk>/', views.PublicacionesDetailView.as_view(), name="publicaciones-detail"),
    path('owned/', views.PublicacionesVendedorListView.as_view(), name="publicaciones-vendedor-list"),
    path('create/', views.publicaciones_create_view, name="publicaciones-create"),
    path('update/<int:pk>/', views.publicaciones_update_view, name="publicaciones-update"),
    path('precio_base/', views.publicaciones_precio_base, name="publicaciones-precio-base"),
    path('precio_minimo/', views.publicaciones_precio_minimo, name="publicaciones-precio-minimo"),
    path('precio_maximo/', views.publicaciones_precio_maximo, name="publicaciones-precio-maximo"),
]