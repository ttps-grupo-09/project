# Generated by Django 2.2 on 2019-12-12 16:17

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('publicaciones', '0002_auto_20191212_1321'),
    ]

    operations = [
        migrations.AlterField(
            model_name='imagen',
            name='url',
            field=models.TextField(max_length=5000),
        ),
    ]
