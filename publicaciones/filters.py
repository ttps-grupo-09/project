import django_filters
from publicaciones.models import Publicacion

class PublicacionFilter(django_filters.FilterSet):
    search = django_filters.CharFilter(method="filter_search")
    categoria = django_filters.NumberFilter(method="filter_categoria")

    min_precio = django_filters.NumberFilter(method="filter_min_precio")
    max_precio = django_filters.NumberFilter(method="filter_max_precio")

    precio_asc = django_filters.BooleanFilter(method="order_by_precio_asc")
    precio_desc = django_filters.BooleanFilter(method="order_by_precio_desc")

    ventas_asc = django_filters.BooleanFilter(method="order_by_ventas_asc")
    ventas_desc = django_filters.BooleanFilter(method="order_by_ventas_desc")

    
    precio_base = django_filters.BooleanFilter(method="filter_precio_base")
    precio_minimo = django_filters.BooleanFilter(method="filter_precio_minimo")
    precio_maximo = django_filters.BooleanFilter(method="filter_precio_maximo")

    estado_activa = django_filters.BooleanFilter(method="filter_estado_activa")
    estado_inactiva = django_filters.BooleanFilter(method="filter_estado_inactiva")
    estado_sin_stock = django_filters.BooleanFilter(method="filter_estado_sin_stock")

    def filter_search(self, queryset, name, value):
        return Publicacion.Search(
            queryset=queryset,
            keywords=value.split(" ")
        )

    def filter_categoria(self, queryset, name, value):
        return queryset.filter(
            producto__categoria__id=value
        )
    
    def filter_min_precio(self, queryset, name, value):
        return Publicacion.annotate_precio(
            queryset=queryset
        ).filter(precio_actual__gte=value)
    
    def filter_max_precio(self, queryset, name, value):
        return Publicacion.annotate_precio(
            queryset=queryset
        ).filter(precio_actual__lte=value)
    
    def order_by_precio_asc(self, queryset, name, value):
        return Publicacion.annotate_precio(
            queryset=queryset
        ).order_by('precio_actual')
    
    def order_by_precio_desc(self, queryset, name, value):
        return Publicacion.annotate_precio(
            queryset=queryset
        ).order_by('-precio_actual')
    
    def order_by_ventas_asc(self, queryset, name, value):
        return queryset.order_by('ventas')
    
    def order_by_ventas_desc(self, queryset, name, value):
        return queryset.order_by('-ventas')

    def filter_precio_base(self, queryset, name, value):
        return queryset.filter(tipo_precio_actual=Publicacion.PRECIO_BASE)
    
    def filter_precio_minimo(self, queryset, name, value):
        return queryset.filter(tipo_precio_actual=Publicacion.PRECIO_MINIMO)
    
    def filter_precio_maximo(self, queryset, name, value):
        return queryset.filter(tipo_precio_actual=Publicacion.PRECIO_MAXIMO)
    
    def filter_estado_activa(self, queryset, name, value):
        return queryset.filter(estado=Publicacion.ACTIVA)
    
    def filter_estado_inactiva(self, queryset, name, value):
        return queryset.filter(estado=Publicacion.INACTIVA)
    
    def filter_estado_sin_stock(self, queryset, name, value):
        return queryset.filter(estado=Publicacion.SIN_STOCK)